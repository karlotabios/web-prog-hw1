<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" initial-scale="1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="style/main.css?v=<?php echo time(); ?>">
    <title>Register</title>
</head>
<body>
	<?php
		$hidden = 1; 
		if ((isset($_POST["patient_name"]) && !empty($_POST["patient_name"])) && (isset($_POST["patient_birthday"]) && !empty($_POST["patient_birthday"])) && (isset($_POST["patient_gender"]) && !empty($_POST["patient_gender"]))) {
			$hidden = 0;
		} else {
			$hidden = 1;
		}
	?>
	<nav>
	</nav>
	<header>
	</header>
	<main class="main">
		<section class="form-section animated fadeIn x-centered">
			<h1 class="element">PATIENT FORM</h1>
			<form class="element" method="POST">
				<div class="row-item">
					<div>
						<label>PATIENT NAME</label>
					</div>
					<input type="text" name="patient_name" />
				</div>
				<div class="row-item">
					<div>
						<label>PATIENT BIRTHDAY</label>
					</div>
					<input type="date" name="patient_birthday" />
				</div>
				<div class="row-item">
					<div>
						<label>GENDER</label>
					</div>
					<input type="radio" name="patient_gender" value="Male" /> Male
					<input type="radio" name="patient_gender" value="Female" /> Female
					<input type="radio" name="patient_gender" value="Other" /> Other
				</div>
				<input type="submit" value="Submit"/>
			</form>
		</section>
		<section class="patient-section x-left-offset animated fadeIn <?php if ($hidden == 1) { echo "hidden"; } ?>" id="patient-list">
			<h1 class="element">MY PATIENT</h1>
				<!-- <?php echo $hidden ?> -->
				<?php
					echo 
						"<div class='element col-wrapper row-element'>" . 
							"<div class='col-item item'>" . 
								"<label class='emphasize'>PATIENT NAME</label>" 
								. "<p>" . ucwords($_POST["patient_name"]) . "</p>"
							 . "</div>"
						 . "<div class='col-item item'>" . 
								"<label class='emphasize'>PATIENT BIRTHDAY</label>"
								. "<p>" . $_POST["patient_birthday"] . "</p>"
							 . "</div>"
						 .  "<div class='col-item item'>" . 
								"<label class='emphasize'>PATIENT GENDER</label>"
								. "<p>" . $_POST["patient_gender"] . "</p>"
							 . "</div>"
						 . "</div>";
				?>
		</section>
	</main>
	<footer>
		<section>
		</section>
		<script type="text/javascript">
			function unhidden(){
				document.getElementById("patient-list").classList.remove('hidden');
			}
		</script>
		<?php 
			function isHidden() {
				if ($hidden == 1) {
					return true;
				} else {
					return false;
				}
			}
		?>
	</footer>
</body>
</html>